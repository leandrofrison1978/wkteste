unit u_TControleCliente;

interface
   uses
    System.Classes, u_TCrud, FireDAC.Comp.Client, u_TCliente;

Type

  TControleCliente = Class(TCrud)
    private
    FCliente: TCliente;
      // virtual - abstract
      function AddWhere(qry: TFDquery): Boolean;override;
      function AddSql(qry: TFDQuery):Boolean;override;
      function AddMemTable(qry : TFDquery):Boolean;override;
      procedure Sincroniza;override;
      //
    public
      property Cliente : TCliente read FCliente write FCliente;

      constructor Create;
      destructor Destroy;override;

  end;

implementation

uses
  System.Types, System.SysUtils, System.StrUtils, u_DmDados;

{ TControleCliente }

function TControleCliente.AddMemTable(qry: TFDquery): Boolean;
begin
  Result := false;
  try
     if not Self.PMemTable.Active then
       Self.PMemTable.Active := True;

     qry.First;
     while not qry.Eof do
     begin
       with Self.PMemTable do begin

         Append;
         FieldByName('CODIGO').AsInteger  := qry.FieldByName('CODIGO').AsInteger;
         FieldByName('NOME').AsString     := qry.FieldByName('NOME').AsString;
         FieldByName('CIDADE').AsString   := qry.FieldByName('CIDADE').AsString;
         FieldByName('UF').AsString       := qry.FieldByName('UF').AsString;
         Post;
       end;
        qry.Next;
     end;

  except
      // TRATAR EXE��ES
  end;
end;

function TControleCliente.AddSql(qry: TFDQuery): Boolean;
begin
  try
    with qry.SQL do begin
      Clear;
      Add('SELECT * FROM CLIENTES');
      Add('WHERE (CODIGO > 0) ');
      Result := True;
    end;
  except
      Result := false;
      // tratar execpt
  end;
end;

function TControleCliente.AddWhere(qry: TFDquery): Boolean;
var
  StringSeparada  : TStringDynArray;
  i, j, n         : integer;
  sparam,spar, pValue, orderby, pory : string;
begin

  Result := false;
  try                 // verificando os parametros para where -
    orderby := 'ORDER BY CODIGO ';
    for i := 0 to Parametros.Count -1 do
    begin
       sparam := LowerCase(Parametros.Items[i].PName);
       pValue := Parametros.Items[i].Value;

       if not pValue.IsEmpty then begin
         if sparam = 'codigo'  then
         begin
           qry.SQL.Add('AND (CODIGO =:p1) ');
           qry.Params.ParamByName('p1').AsInteger := TextToInt(pValue);
         end
         else
         if sparam = 'nome'  then
         begin
           StringSeparada := SplitString(pValue,'+');
           for j := 0 to High(StringSeparada) do
           begin
              spar := 'pn'+ IntToText(j);
              qry.SQL.Add(' AND (UPPER(NOME) LIKE :'+spar+') ');
              qry.Params.ParamByName(spar).AsString := '%'+
                                    Trim(UpperCase(StringSeparada[j]))+'%';
           end;
         end
         else
         if sparam = 'orderby'  then
         begin
           pory           := UpperCase(pValue);
           orderby        := 'ORDER BY ';
           StringSeparada := SplitString(pValue,'+');

           for j := 0 to High(StringSeparada) do
              orderby := orderby +' '+ Trim(UpperCase(StringSeparada[j]));

         end;
       end;
    end;
    qry.SQL.Add(orderby);
    Result := True;
  except
     sleep(1);
  end;

end;

constructor TControleCliente.Create;
begin
  inherited Create(dm_Dados.FDConnection,dm_dados.tbClientes);
  FCliente  := TCliente.Create;
  Self.Default;
end;

destructor TControleCliente.Destroy;
begin
  if Assigned(FCliente) then FreeAndNil(FCliente);

  inherited;
end;

procedure TControleCliente.Sincroniza;
begin
  inherited;
  Cliente.Default;
  with Cliente do begin
    Codigo    := PMemTable.FieldByName('CODIGO').AsInteger;
    Nome      := PMemTable.FieldByName('NOME').AsString;
    Cidade    := PMemTable.FieldByName('CIDADE').AsString;
    UF        := PMemTable.FieldByName('UF').AsString;
  end;
end;

end.
