unit u_TControlePedidos;

interface
   uses
    System.Classes, u_TCrud, FireDAC.Comp.Client, u_TPedido,
  u_TControleProdutosPedido;

Type

  TControlePedidos = Class(TCrud)
    private
    FPedido: TPedido;
    FItensPedido: TControleItens;
      // virtual - abstract
      function AddWhere(qry: TFDquery): Boolean;override;
      function AddSql(qry: TFDQuery):Boolean;override;
      function AddMemTable(qry : TFDquery):Boolean;override;
      function AddSqlAccept(qry1,qry2 : TFDQuery):Boolean;override;
      procedure Sincroniza;override;
      //
    public
      property Pedido : TPedido read FPedido write FPedido;
      property ItensPedido : TControleItens read FItensPedido write FItensPedido;

      constructor Create;
      destructor Destroy;override;

      function Novo: Boolean;
      function SalvarRegistro: Boolean;

  end;

implementation

uses
  System.Types, System.SysUtils, System.StrUtils, u_DmDados, Data.DB;

{ TControleCliente }

function TControlePedidos.AddMemTable(qry: TFDquery): Boolean;
begin
  Result := false;
  try
     if not Self.PMemTable.Active then
       Self.PMemTable.Active := True;

     Self.ZerarValores;

     qry.First;
     while not qry.Eof do
     begin
       with Self.PMemTable do begin
         Append;
         FieldByName('NUMEROPEDIDO').AsInteger      :=
                    qry.FieldByName('NUMEROPEDIDO').AsInteger;
         FieldByName('DATAEMISSAO').AsDateTime      :=
                    qry.FieldByName('DATAEMISSAO').AsDateTime;
         FieldByName('CODIGOCLIENTE').AsInteger     :=
                    qry.FieldByName('CODIGOCLIENTE').AsInteger;
         FieldByName('VALORTOTAL').AsCurrency       :=
                    qry.FieldByName('VALORTOTAL').AsCurrency;
         FieldByName('NOMECLIENTE').AsString        :=
                    qry.FieldByName('NOMECLIENTE').AsString;
         FieldByName('IDGUID').AsString        :=
                    qry.FieldByName('IDGUID').AsString;
         // NOME CLIENTE
         Post;
         Self.AddValor(eSubtotal,FieldByName('VALORTOTAL').AsCurrency);

       end;
        qry.Next;
     end;

  except
      // TRATAR EXE��ES
  end;
end;

function TControlePedidos.AddSql(qry: TFDQuery): Boolean;
begin
  try
    with qry.SQL do begin
      Clear;
      Add('SELECT * FROM VW_PEDIDOS');
      Add('WHERE (NUMEROPEDIDO > 0) ');
      Result := True;
    end;
  except
      Result := false;
      // tratar execpt
  end;
end;

function TControlePedidos.AddSqlAccept(qry1, qry2: TFDQuery): Boolean;
var
  i, id : integer;
  IDPedido : string;
begin
  Result := false;
  try

    // gerando o codigo de identifica��o universal
    IDPedido  := Self.GuidCreate;
    // transa��o
    Self.Conexao.StartTransaction;
    try

      with qry1.SQL do
      begin

       Clear;
       Add('INSERT INTO PEDIDOS(CODIGOCLIENTE, VALORTOTAL,IDGUID) ');
       Add('VALUES(:p1,:p2,:p3) ');
      end;
      // PARAMETROS
      with qry1.Params do begin
        ParamByName('p1').AsInteger   := Self.Pedido.CodigoCliente;
        ParamByName('p2').AsCurrency  := Self.Pedido.ValotTotal;
        ParamByName('P3').AsString    := IDPedido;
      end;

      qry1.ExecSQL;

      Self.Conexao.Commit;
      Sleep(100);
      // tudo certo inicia a transa��o dos itens de pedido


        with qry2.SQL do
        begin
          Clear;
          Add('INSERT INTO PEDIDOSPRODUTOS(IDGUID,CODIGOPRODUTO, ');
          Add('QUANTIDADE, VLRUNITARIO, VLRTOTAL ) ');
          Add('VALUES(:p1,:p2,:p3,:p4,:p5) ');
        end;
        // PARAMETROS
        with Self.ItensPedido.PMemTable do begin
         First;
         while not Eof do begin

          with qry2.Params do begin
            ParamByName('p1').AsString    := IDPedido;
            ParamByName('p2').AsInteger   := FieldByName('CODIGOPRODUTO').AsInteger;
            ParamByName('p3').AsInteger   := FieldByName('QUANTIDADE').AsInteger;
            ParamByName('p4').AsCurrency  := FieldByName('VLRUNITARIO').AsCurrency;
            ParamByName('p5').AsCurrency  := FieldByName('VLRTOTAL').AsCurrency;
          end;
          Self.Conexao.StartTransaction;
          qry2.ExecSQL;
          Self.Conexao.Commit;
          Next;
         end;
        end;

        Result := True;

    except on E:Exception do
      begin
        Result := False;
        Self.Conexao.Rollback;
      end;
    end;


  except
     Sleep(1);
  end;

end;

function TControlePedidos.AddWhere(qry: TFDquery): Boolean;
var
  StringSeparada  : TStringDynArray;
  i, j, n         : integer;
  sparam,spar, pValue, orderby, pory : string;
begin

  Result := false;
  try                 // verificando os parametros para where -
    orderby := 'ORDER BY NUMEROPEDIDO ';
    for i := 0 to Parametros.Count -1 do
    begin
       sparam := LowerCase(Parametros.Items[i].PName);
       pValue := Parametros.Items[i].Value;

       if not pValue.IsEmpty then begin
         if sparam = 'numeropedido'  then
         begin
           qry.SQL.Add('AND (NUMEROPEDIDO =:p1) ');
           qry.Params.ParamByName('p1').AsInteger := TextToInt(pValue);
         end
         else
         if sparam = 'codigocliente'  then
         begin
           StringSeparada := SplitString(pValue,'+');
           for j := 0 to High(StringSeparada) do
           begin
              spar := 'pn'+ IntToText(j);
              qry.SQL.Add(' AND (UPPER(DESCRICAO) LIKE :'+spar+') ');
              qry.Params.ParamByName(spar).AsString := '%'+
                                    Trim(UpperCase(StringSeparada[j]))+'%';
           end;
         end
         else
         if sparam = 'orderby'  then
         begin
           pory           := UpperCase(pValue);
           orderby        := 'ORDER BY ';
           StringSeparada := SplitString(pValue,'+');

           for j := 0 to High(StringSeparada) do
              orderby := orderby +' '+ Trim(UpperCase(StringSeparada[j]));

         end;
       end;
    end;
    qry.SQL.Add(orderby);
    Result := True;
  except

  end;

end;

constructor TControlePedidos.Create;
begin
  inherited Create(dm_Dados.FDConnection,dm_dados.tbPedidos);
  FPedido      := TPedido.Create;
  FItensPedido := TControleItens.Create;
  // carregando a tabela vazia
  ItensPedido.Parametros.Clear;
  ItensPedido.AddParametro('tipo','lista');
  ItensPedido.GetLista;
  Self.Default;
end;

destructor TControlePedidos.Destroy;
begin
  if Assigned(FItensPedido) then FreeAndNil(FItensPedido);
   if Assigned(FPedido) then FreeAndNil(FPedido);
  inherited;
end;

function TControlePedidos.Novo: Boolean;
begin
  Result := false;
  try
     Self.Status := dsInsert;
     Self.Pedido.Default;
     Self.ItensPedido.Default;
     Self.ZerarValores;


  except

  end;
end;

function TControlePedidos.SalvarRegistro: Boolean;
begin
  try
    case Status of
      dsBrowse: begin

      end;
      dsEdit: begin
         Self.Parametros.Clear;
         Self.AddParametro('tipo','accept');
         Self.Total := Self.ItensPedido.Total;
         Result := Self.Salvar;
      end;
      dsInsert: begin
        Self.Parametros.Clear;
        Self.AddParametro('tipo','accept');
        Self.Total := Self.ItensPedido.Total;
        Result := Self.Salvar;
      end;
    end;

  except
    Result := false;
  end;
end;

procedure TControlePedidos.Sincroniza;
begin
  inherited;
  try
    with Self.PMemTable do begin

         Pedido.NumPedido     := FieldByName('NUMEROPEDIDO').AsInteger;
         Pedido.CodigoCliente := FieldByName('CODIGOCLIENTE').AsInteger;
         Pedido.ValotTotal    := FieldByName('VALORTOTAL').AsCurrency;
         Pedido.Cliente       := FieldByName('NOMECLIENTE').AsString;
         Pedido.IdGuid        := FieldByName('IDGUID').AsString;


    end;
  except

  end;
end;

end.
