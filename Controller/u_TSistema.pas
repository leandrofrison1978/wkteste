unit u_TSistema;

interface
  uses
    System.Classes, u_TControleCliente, u_TControlePedidos, u_TControleProdutos,
  u_TControleProdutosPedido;

Type
  TSystema = class
    private
    FClientes: TControleCliente;
    FProdutos: TControleProdutos;
    FPedidos: TControlePedidos;

    public
    property Clientes : TControleCliente read FClientes write FClientes;
    property Produtos : TControleProdutos read FProdutos write FProdutos;
    property Pedidos : TControlePedidos read FPedidos write FPedidos;

    constructor Create;
    destructor Destroy;override;
  end;
implementation

uses
  System.SysUtils;

{ TSystema }

constructor TSystema.Create;
begin
  inherited;
  FClientes   := TControleCliente.Create;
  FProdutos   := TControleProdutos.Create;
  FPedidos    := TControlePedidos.Create;
end;

destructor TSystema.Destroy;
begin
  if Assigned(FClientes) then Freeandnil(FClientes);
  if Assigned(FPedidos) then Freeandnil(FPedidos);
  if Assigned(FProdutos) then Freeandnil(FProdutos);
  inherited;
end;

end.
