unit u_TControleProdutosPedido;

interface
   uses
    System.Classes, u_TCrud, FireDAC.Comp.Client, u_TProdutoPediddo;

Type

  TControleItens = Class(TCrud)
    private
    FItenPedido: TItensPedido;
      // virtual - abstract
      function AddWhere(qry: TFDquery): Boolean;override;
      function AddSql(qry: TFDQuery):Boolean;override;
      function AddMemTable(qry : TFDquery):Boolean;override;
      function ExecuteDelete:Boolean;override;
      procedure Sincroniza;override;
      //
    public
      property ItenPedido : TItensPedido read FItenPedido write FItenPedido;

      function AddProduto(const codprod, qtd, vlunit, vltotal,descricacao : string):Boolean;
      constructor Create;
      destructor Destroy;override;

  end;

implementation

uses
  System.Types, System.SysUtils, System.StrUtils, u_DmDados;

{ TControleCliente }

function TControleItens.AddMemTable(qry: TFDquery): Boolean;
begin
  Result := false;
  try
     if not Self.PMemTable.Active then
       Self.PMemTable.Active := True;

     Self.ZerarValores;

     qry.First;
     while not qry.Eof do
     begin
       with Self.PMemTable do begin
         Append;
         FieldByName('AUTOINCREM').AsInteger      := qry.FieldByName('AUTOINCREM').AsInteger;
         FieldByName('NUMEROPEDIDO').AsInteger    := qry.FieldByName('NUMEROPEDIDO').AsInteger;
         FieldByName('CODIGOPRODUTO').AsInteger   := qry.FieldByName('CODIGOPRODUTO').AsInteger;
         FieldByName('QUANTIDADE').AsInteger      := qry.FieldByName('QUANTIDADE').AsInteger;
         FieldByName('VLRUNITARIO').AsCurrency    := qry.FieldByName('VLRUNITARIO').AsCurrency;
         FieldByName('DESCRICAO').AsString        := qry.FieldByName('DESCRICAO').AsString;
         FieldByName('VLRTOTAL').AsCurrency       := qry.FieldByName('VLRTOTAL').AsCurrency;
         FieldByName('IDGUID').AsString           := qry.FieldByName('IDGUID').AsString;
         Post;

         // Somando os valores totais
         Self.AddValor(eSubtotal,FieldByName('VLRTOTAL').AsCurrency);
       end;
        qry.Next;
     end;

  except
      // TRATAR EXE��ES
  end;
end;

function TControleItens.AddProduto(const codprod, qtd, vlunit,
  vltotal,descricacao: string): Boolean;
begin
  if not((codprod.IsEmpty)and(vlunit.IsEmpty)and(vltotal.IsEmpty)) then
  begin
    with PMemTable do begin
      Append;
      FieldByName('CODIGOPRODUTO').AsInteger  := Self.TextToInt(codprod);
      FieldByName('VLRUNITARIO').AsCurrency   := Self.TextToCur(vlunit);
      FieldByName('VLRTOTAL').AsCurrency      := Self.TextToCur(vltotal);
      FieldByName('QUANTIDADE').AsInteger     := sELF.TextToInt(qtd);
      FieldByName('DESCRICAO').AsString       := descricacao;
      Post;
      // somatoria
      Self.AddValor(eSubtotal,FieldByName('VLRTOTAL').AsCurrency);
      Self.SomarTotal;

      Result := True;
    end;
  end;
end;

function TControleItens.AddSql(qry: TFDQuery): Boolean;
begin
  try
    with qry.SQL do begin
      Clear;
      Add('SELECT * FROM VW_PRODUTOS_PEDIDO');
      Add('WHERE (VLRTOTAL > -1) ');
      Result := True;
    end;
  except
      Result := false;
      // tratar execpt
  end;
end;

function TControleItens.AddWhere(qry: TFDquery): Boolean;
var
  StringSeparada  : TStringDynArray;
  i, j, n         : integer;
  sparam,spar, pValue, orderby, pory : string;
begin

  Result := false;
  try                 // verificando os parametros para where -
    orderby := 'ORDER BY VLRTOTAL ';
    for i := 0 to Parametros.Count -1 do
    begin
       sparam := LowerCase(Parametros.Items[i].PName);
       pValue := Parametros.Items[i].Value;

       if not pValue.IsEmpty then begin
         if sparam = 'autoincrem'  then
         begin
           qry.SQL.Add('AND (AUTOINCREM =:p1) ');
           qry.Params.ParamByName('p1').AsInteger := TextToInt(pValue);
         end
         else
         if sparam = 'numeropedido'  then
         begin
           qry.SQL.Add('AND (NUMEROPEDIDO =:p2) ');
           qry.Params.ParamByName('p2').AsInteger := TextToInt(pValue);
         end
         else
         if sparam = 'codigocliente'  then
         begin
           qry.SQL.Add('AND (CODIGOCLIENTE =:p3) ');
           qry.Params.ParamByName('p3').AsInteger := TextToInt(pValue);
         end
         else
         if sparam = 'descricao'  then
         begin
           StringSeparada := SplitString(pValue,'+');
           for j := 0 to High(StringSeparada) do
           begin
              spar := 'pn'+ IntToText(j);
              qry.SQL.Add(' AND (UPPER(DESCRICAO) LIKE :'+spar+') ');
              qry.Params.ParamByName(spar).AsString := '%'+
                                    Trim(UpperCase(StringSeparada[j]))+'%';
           end;
         end
         else
         if sparam = 'orderby'  then
         begin
           pory           := UpperCase(pValue);
           orderby        := 'ORDER BY ';
           StringSeparada := SplitString(pValue,'+');

           for j := 0 to High(StringSeparada) do
              orderby := orderby +' '+ Trim(UpperCase(StringSeparada[j]));

         end;
       end;
    end;
    qry.SQL.Add(orderby);
    Result := True;
  except

  end;

end;

constructor TControleItens.Create;
begin
  inherited Create(dm_Dados.FDConnection,dm_dados.tbProdutosPedido);
  FItenPedido := TItensPedido.Create;
  Self.PMemTable.Active := True;
  Self.Default;
end;

destructor TControleItens.Destroy;
begin
  if Assigned(FItenPedido) then FreeAndNil(FItenPedido);

  inherited;
end;

function TControleItens.ExecuteDelete: Boolean;
var
  qry : TFDQuery;
  idproduto : integer;
  valor : Currency;
begin
  Result := False;
  qry := TFDQuery.Create(nil);
  Self.Conexao.StartTransaction;
  try
    try
       Self.ConectaQry(qry);

       qry.SQL.Add('DELETE FROM PEDIDOSPRODUTOS WHERE AUTOINCREM =:p1 ');
       qry.Params.ParamByName('p1').AsInteger := PMemTable.FieldByName('AUTOINCREM').AsInteger;

       qry.ExecSQL;

       if qry.RowsAffected > 0 then
       begin
          Self.Conexao.Commit;
          valor :=  PMemTable.FieldByName('VLRTOTAL').AsCurrency;
          Self.SubTotal := SubTotal - valor;
          Self.Total := SomarTotal;
          Self.PMemTable.Delete;
          Result := True;
       end
        else
            Self.Conexao.Rollback;

    except on E:Exception do
      Self.Conexao.Rollback;

    end;
  finally
    if Assigned(qry) then FreeAndNil(qry);

  end;
end;

procedure TControleItens.Sincroniza;
begin
  inherited;
  try
     if PMemTable.RecordCount > 0 then begin

       with Self.ItenPedido do begin
         Default;
         AutoIncrem     := PMemTable.FieldByName('AUTOINCREM').AsInteger;
         NumeroPedido   := PMemTable.FieldByName('NUMEROPEDIDO').AsInteger;
         Quantidade     := PMemTable.FieldByName('QUANTIDADE').AsInteger;
         CodigoProduto  := PMemTable.FieldByName('CODIGOPRODUTO').AsInteger;
         VlrUnitario    := PMemTable.FieldByName('VLRUNITARIO').AsCurrency;
         VlrTotal       := PMemTable.FieldByName('VLRTOTAL').AsCurrency;
         Descricao      := PMemTable.FieldByName('DESCRICAO').AsString;
         IdGuid         := PMemTable.FieldByName('IDGUID').AsString;
       end;
     end;
  except

  end;
end;

end.
