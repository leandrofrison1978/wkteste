CREATE DATABASE  IF NOT EXISTS `dadosdb` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `dadosdb`;
-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: dadosdb
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `vw_pedidos`
--

DROP TABLE IF EXISTS `vw_pedidos`;
/*!50001 DROP VIEW IF EXISTS `vw_pedidos`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_pedidos` AS SELECT 
 1 AS `NUMEROPEDIDO`,
 1 AS `DATAEMISSAO`,
 1 AS `CODIGOCLIENTE`,
 1 AS `VALORTOTAL`,
 1 AS `NOMECLIENTE`,
 1 AS `IDGUID`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_produtos_pedido`
--

DROP TABLE IF EXISTS `vw_produtos_pedido`;
/*!50001 DROP VIEW IF EXISTS `vw_produtos_pedido`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_produtos_pedido` AS SELECT 
 1 AS `NUMEROPEDIDO`,
 1 AS `CODIGOPRODUTO`,
 1 AS `QUANTIDADE`,
 1 AS `VLRUNITARIO`,
 1 AS `VLRTOTAL`,
 1 AS `DESCRICAO`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `vw_pedidos`
--

/*!50001 DROP VIEW IF EXISTS `vw_pedidos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_pedidos` (`NUMEROPEDIDO`,`DATAEMISSAO`,`CODIGOCLIENTE`,`VALORTOTAL`,`NOMECLIENTE`,`IDGUID`) AS select `a`.`numeropedido` AS `NUMEROPEDIDO`,`a`.`dataemissao` AS `DATAEMISSAO`,`a`.`codigocliente` AS `CODIGOCLIENTE`,`a`.`valortotal` AS `VALORTOTAL`,`b`.`Nome` AS `NOME`,`a`.`idguid` AS `IDGUID` from (`pedidos` `a` left join `clientes` `b` on((`b`.`Codigo` = `a`.`codigocliente`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_produtos_pedido`
--

/*!50001 DROP VIEW IF EXISTS `vw_produtos_pedido`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_produtos_pedido` (`NUMEROPEDIDO`,`CODIGOPRODUTO`,`QUANTIDADE`,`VLRUNITARIO`,`VLRTOTAL`,`DESCRICAO`) AS select `a`.`numeropedido` AS `NUMEROPEDIDO`,`a`.`codigoproduto` AS `CODIGOPRODUTO`,`a`.`quantidade` AS `QUANTIDADE`,`a`.`vlrunitario` AS `VLRUNITARIO`,`a`.`vlrtotal` AS `VLRTOTAL`,`b`.`Descricao` AS `DESCRICAO` from (`pedidosprodutos` `a` left join `produtos` `b` on((`b`.`Codigo` = `a`.`codigoproduto`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'dadosdb'
--

--
-- Dumping routines for database 'dadosdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-10 12:58:06
