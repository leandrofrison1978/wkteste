unit u_TCrud;

interface
   uses System.Classes, FireDAC.Comp.Client, System.Generics.Collections,
   Data.DB, Vcl.StdCtrls,ComObj, ActiveX;
type
  TEnumCalc = (eSubtotal, eTotal, eDesconto, eAcressimo);


  TParametro = class
    private
    FValue: string;
    FPName: string;

    public
    property PName : string read FPName write FPName;
    property Value : string read FValue write FValue;

    constructor Create;
    procedure Default;
  end;

  TCrud = class
    private
    FNItems: integer;
    FConexao: TFDConnection;
    FParametros: TObjectList<TParametro>;
    FTipo: string;
    FPMemTable: TFDMemTable;
    FIdGuid: string;
    FSubTotal: Currency;
    FTotal: Currency;
    FDesconto: Currency;
    FAcressimo: Currency;
    FStatus: TDataSetState;



    procedure SetStatus(const Value: TDataSetState);

    public
    property NItems : integer read FNItems write FNItems;
    property SubTotal : Currency read FSubTotal write FSubTotal;
    property Total : Currency read FTotal write FTotal;
    property Desconto : Currency read FDesconto write FDesconto;
    property Acressimo : Currency read FAcressimo write FAcressimo;
    property Conexao : TFDConnection read FConexao write FConexao;
    property Parametros : TObjectList<TParametro> read FParametros write FParametros;
    property Tipo : string read FTipo write FTipo;
    property PMemTable : TFDMemTable read FPMemTable write FPMemTable;
    property IdGuid : string read FIdGuid write FIdGuid;
    property Status : TDataSetState read FStatus write SetStatus;
    // virtual - abstract
    function AddWhere(qry: TFDquery): Boolean;virtual;abstract;
    function AddSql(qry: TFDQuery):Boolean;virtual;abstract;
    function AddMemTable(qry : TFDquery):Boolean;virtual;abstract;
    function AddSqlAccept(qry1,qry2 : TFDQuery):Boolean;virtual;abstract;
    function ExecuteDelete:Boolean;virtual;abstract;
    procedure Sincroniza;virtual;abstract;
    //
    function ConectaQry(qry : TFDQuery):Boolean;
    function GetLista: string;
    function Salvar: Boolean;
    function Delete:Boolean;
    function Localizar(const Campo, Valor : string): boolean;
    //
    function TextToInt(const Value : string =''): integer;
    function IntToText(const Value : integer = 0):string;
    function TextToCur(const Value : string = ''):Currency;
    function CurrToText(const Value : Currency; Display : string):string;
    function SomarTotal: Currency;

    procedure AddValor(pOperacao : TEnumCalc; const Value : Currency = 0);

    procedure ZerarValores;
    procedure LimparMemTable;
    function GetParametro(const Name : string =''):string;
    function GuidCreate: string;
    procedure AddParametro(const Name, Valor: string);
    procedure CarregarCombo(pCombo : TComboBox; const sCampo : string);

    constructor Create(AConexao : TFDConnection; AMemTable : TFDMemTable);
    procedure Default;
    destructor Destroy;override;


  end;
implementation

uses
  System.SysUtils;

{ TCrud }

procedure TCrud.AddParametro(const Name, Valor: string);
begin
  try
     if not Name.IsEmpty then begin

       Self.Parametros.Add(TParametro.Create);
       with Self.Parametros.Last do
       begin
         PName  := Name;
         Value  := Valor;
       end;
     end;
  except

  end;
end;

procedure TCrud.AddValor(pOperacao: TEnumCalc; const Value: Currency);
begin
  if Value > 0 then begin

    case pOperacao of
      eSubtotal: FSubTotal    := SubTotal + Value;
      eTotal: ;
      eDesconto: FDesconto    := Desconto + Value;
      eAcressimo: FAcressimo  := Acressimo + Value;
    end;
      Total := Self.SomarTotal;
  end;
end;

procedure TCrud.CarregarCombo(pCombo: TComboBox; const sCampo: string);
begin
  try
       if Self.PMemTable.State = dsBrowse then
       begin
          pCombo.Clear;
          pCombo.Style := csDropDownList;
          Self.PMemTable.DisableControls;
          Self.PMemTable.First;
          while not(Self.PMemTable.Eof) do
          begin
             pCombo.Items.Add(Self.PMemTable.FieldByName(sCampo).AsString);
             Self.PMemTable.Next;
          end;
       end;
   finally
      Self.PMemTable.EnableControls;
   end;
end;

procedure TCrud.LimparMemTable;
begin
  Self.PMemTable.Active := false;
  Self.PMemTable.Active := True;
end;

function TCrud.ConectaQry(qry: TFDQuery): Boolean;
begin
  Result := False;
  try

    qry.Active      := false;
    qry.SQL.Clear;
    qry.Connection  := Conexao;
    qry.Connection.Connected;

    Result := True;
  except

  end;
end;

constructor TCrud.Create(AConexao : TFDConnection; AMemTable : TFDMemTable);
begin
  FConexao    := AConexao;
  FPMemTable  := AMemTable;
  FParametros := TObjectList<TParametro>.Create;
  Self.Default;
end;

function TCrud.CurrToText(const Value: Currency; Display: string): string;
var
  sResult : string;
begin
  try
     if not Display.IsEmpty then
       sResult := Display
     else
      sResult := ' ';

     Result := sResult+ '  ' + FormatCurr('##0.00',Value);
  except
     Result := sResult + '  0,00';
  end;
end;

procedure TCrud.Default;
begin
  FNItems     := 0;
  FTipo       := '';
  FIdGuid     := '';
  FStatus     := dsBrowse;
  Self.LimparMemTable;
  Self.ZerarValores;

end;

function TCrud.Delete: Boolean;
const
  msgerro : string ='Erro: Deletar registro: parametros incorretos!';
  msginvalido : string ='Erro: Parametros do SQL invalidos!';
begin
     try
          Result := Self.ExecuteDelete
     except  on E:Exception do
      Result := False;
     end;


end;

destructor TCrud.Destroy;
begin
  if Assigned(FParametros) then FreeandNil(FParametros);

  inherited;
end;

function TCrud.GetLista: string;
var
  qry   : TFDQuery;
  pid   : integer;
begin
  // retornas todos  cadastrados no sistema
  qry   := TFDQuery.Create(Nil);
  try
    try
      if Self.ConectaQry(qry) then begin                    // conectado

       Tipo := Self.GetParametro('tipo');

       if ((Tipo = 'lista') or (Tipo ='solo')) then begin  // parametros aceito

         if Self.AddSql(qry) then
          if Self.AddWhere(qry) then
          begin
            qry.open;

            if qry.RecordCount > 0 then
            begin
               PMemTable.Active := false;
               PMemTable.Active := True;
               qry.First;
               Self.PMemTable.DisableControls;
               Self.PMemTable.Active := false;
               Self.PMemTable.Active := True;
               Self.AddMemTable(qry);
               Self.PMemTable.EnableControls;
            end
            else
               Result := 'Nenhum registro encontrado!';
          end
          else
               Result := 'Verifique os parametros informados!';
       end;
      end;


    except on EX:Exception do
      begin
         Result :='Falha geral';
      end;

    end;
  finally
    if Assigned(qry) then FreeAndNil(qry);
    Sleep(1);
  end;

end;

function TCrud.GetParametro(const Name: string): string;
var
  i : integer;
begin
   if not Name.IsEmpty then begin

     for i := 0 to Self.Parametros.Count -1 do begin

       if Self.Parametros.Items[i].PName = Name then begin

         Result := Self.Parametros.Items[i].Value;
         Break;
       end;
     end;

   end;
end;

function TCrud.GuidCreate: string;
var
  ID : TGUID;
begin
   // identificador unico global
   Result := '';
   if CoCreateGuid(ID) = S_OK then
     Result := GUIDToString(ID);

end;

function TCrud.IntToText(const Value: integer): string;
begin
   try
      Result := IntToStr(Value);
   except
      Result := '0';
   end;
end;

function TCrud.Localizar(const Campo, Valor: string): boolean;
begin
  Result := false;
  Self.PMemTable.DisableControls;
  try
    try
       if not((Campo.IsEmpty)and(Valor.IsEmpty)) then begin

         Self.PMemTable.First;

         if PMemTable.Locate(UpperCase(Campo),Valor,[loCaseInsensitive,loPartialkey]) then
            Result := True;

       end;
    except

    end;
  finally
    Self.PMemTable.EnableControls;
  end;

end;

function TCrud.Salvar: Boolean;
var
  qry1, qry2  : TFDQuery;
begin
   Result := false;
   qry1   := TFDQuery.Create(nil);
   qry2   := TFDQuery.Create(nil);

   try
     try
       Self.ConectaQry(qry1);
       Self.ConectaQry(qry2);

        if (Self.GetParametro('tipo') = 'accept') then
        begin
            // AddSqlAccept controla a trans��o
           if not AddSqlAccept(qry1,qry2) then
             Result := false
           else
              Result := True;
        end;

     except  on E:Exception do
      begin
        Result := false;
        // tratar erros aqui
      end;
     end;
   finally
     if Assigned(qry1)   then FreeAndNil(qry1);
     if Assigned(qry2)   then FreeAndNil(qry2);
     Sleep(1);
   end;


end;


procedure TCrud.SetStatus(const Value: TDataSetState);
begin
  FStatus := Value;
end;

function TCrud.SomarTotal: Currency;
begin
  try
     Result := ((SubTotal + Acressimo) - Desconto);
  except
     Result := 0;
  end;
end;

function TCrud.TextToCur(const Value: string): Currency;
begin
  try
     if not Value.IsEmpty then
        Result := StrToCurr(Value);
   except
      Result := 0;
   end;
end;

function TCrud.TextToInt(const Value: string): integer;
begin
  try
     if not Value.IsEmpty then
        Result := StrToInt(Value);
   except
      Result := -1;
   end;
end;

procedure TCrud.ZerarValores;
begin
  // valores totais
     Self.SubTotal  := 0;
     Self.Desconto  := 0;
     Self.Acressimo := 0;
     Self.Total     := 0;
end;

{ TParams }

constructor TParametro.Create;
begin
  inherited;
  Self.Default;
end;

procedure TParametro.Default;
begin
  Self.PName    := '';
  Self.Value    := '';
end;

end.
