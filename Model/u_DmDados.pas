unit u_DmDados;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.Comp.DataSet,
  Data.DBXMySQL, Data.SqlExpr, FireDAC.Comp.UI, FireDAC.Stan.StorageBin,
  FireDAC.DApt;

type
  Tdm_Dados = class(TDataModule)
    FDConnection: TFDConnection;
    FDPhysMySQLDriverLink2: TFDPhysMySQLDriverLink;
    tbClientes: TFDMemTable;
    tbClientesCODIGO: TIntegerField;
    tbClientesNOME: TStringField;
    tbClientesCIDADE: TStringField;
    tbClientesUF: TStringField;
    tbProdutos: TFDMemTable;
    tbPedidos: TFDMemTable;
    tbProdutosPedido: TFDMemTable;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    FDQuery1: TFDQuery;
    tbProdutosCODIGO: TIntegerField;
    tbProdutosDESCRICAO: TStringField;
    tbProdutosPRECOVENDA: TCurrencyField;
    tbPedidosNUMEROPEDIDO: TIntegerField;
    tbPedidosDATAEMISSAO: TDateField;
    tbPedidosVALORTOTAL: TCurrencyField;
    tbPedidosCODIGOCLIENTE: TIntegerField;
    tbProdutosPedidoNUMEROPEDIDO: TIntegerField;
    tbProdutosPedidoCODIGOPRODUTO: TIntegerField;
    tbProdutosPedidoQUANTIDADE: TIntegerField;
    tbProdutosPedidoVLRUNITARIO: TCurrencyField;
    tbProdutosPedidoVLRTOTAL: TCurrencyField;
    tbProdutosPedidoDESCRICAO: TStringField;
    tbPedidosNOMECLIENTE: TStringField;
    tbPedidosIDGUID: TStringField;
    FDQuery1NUMEROPEDIDO: TFDAutoIncField;
    FDQuery1DATAEMISSAO: TSQLTimeStampField;
    FDQuery1CODIGOCLIENTE: TIntegerField;
    FDQuery1VALORTOTAL: TBCDField;
    FDQuery1NOMECLIENTE: TStringField;
    FDQuery1IDGUID: TStringField;
    tbProdutosPedidoIDGUID: TStringField;
    tbProdutosPedidoAUTOINCREM: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm_Dados: Tdm_Dados;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

end.
