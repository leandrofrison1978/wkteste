unit u_TProduto;

interface
  uses System.Classes;

  Type
    TProduto = class
      private
      FDescricao: string;
      FCodigo: integer;
      FPrecoVenda: Currency;

      public
      property Codigo : integer read FCodigo write FCodigo;
      property Descricao : string read FDescricao write FDescricao;
      property PrecoVenda : Currency read FPrecoVenda write FPrecoVenda;

      constructor Create;
      procedure Default;
    end;

implementation

{ TControleProduto }

constructor TProduto.Create;
begin
  inherited;
  Self.Default;
end;

procedure TProduto.Default;
begin
  FCodigo     := -1;
  FDescricao  := '';
  FPrecoVenda := 0;
end;

end.
