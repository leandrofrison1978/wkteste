unit u_TProdutoPediddo;

interface
   uses System.Classes;

Type
  TItensPedido = class
    private
    FAutoIncrem     : integer;
    FVlrTotal       : Currency;
    FCodigoProduto  : integer;
    FNumeroPedido   : integer;
    FVlrUnitario    : Currency;
    FDescricao: string;
    FQuantidade: integer;

    public
    property AutoIncrem : integer read FAutoIncrem write FAutoIncrem;
    property NumeroPedido : integer  read FNumeroPedido write FNumeroPedido;
    property CodigoProduto : integer read FCodigoProduto write FCodigoProduto;
    property VlrUnitario : Currency read FVlrUnitario write FVlrUnitario;
    property VlrTotal : Currency read FVlrTotal write FVlrTotal;
    property Descricao : string  read FDescricao write FDescricao;
    property Quantidade : integer read FQuantidade write FQuantidade;

    constructor Create;
    procedure Default;
  end;

implementation

{ TItensPedido }

constructor TItensPedido.Create;
begin
  inherited;
  Self.Default;
end;

procedure TItensPedido.Default;
begin

  FAutoIncrem     := -1;
  FVlrTotal       := 0;
  FCodigoProduto  := -1;
  FNumeroPedido   := -1;
  FVlrUnitario    := 0;
  FDescricao      := '';
  FQuantidade     := 0;
end;

end.
