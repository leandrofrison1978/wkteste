unit u_TPedido;

interface
  uses
   System.Classes;

Type
  TPedido = class
    private
    FNumPedido: integer;
    FValorTotal: Currency;
    FDataEmissao: TDate;
    FCodigoCliente: integer;
    FIdGuid: string;
    FCliente: string;

    public
    property NumPedido : integer read FNumPedido write FNumPedido;
    property DataEmissao : TDate read FDataEmissao write FDataEmissao;
    property CodigoCliente : integer read FCodigoCliente write FCodigoCliente;
    property ValotTotal : Currency read FValorTotal write FValorTotal;
    property IdGuid : string read FIdGuid write FIdGuid;
    property Cliente : string read FCliente write FCliente;

    constructor Create;
    procedure Default;

  end;
implementation

uses
  System.SysUtils;

{ TPedido }

constructor TPedido.Create;
begin
  inherited;
  Self.Default;
end;

procedure TPedido.Default;
begin
   FNumPedido       := -1;
   FCodigoCliente   := -1;
   FDataEmissao     := Now;
   FValorTotal      := 0;
end;

end.
