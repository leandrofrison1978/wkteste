object dm_Dados: Tdm_Dados
  OldCreateOrder = False
  Height = 311
  Width = 446
  object FDConnection: TFDConnection
    Params.Strings = (
      'Database=dadosdb'
      'User_Name=wkteste'
      'Password=123456789'
      'Server=localhost'
      'DriverID=MySQL')
    Connected = True
    LoginPrompt = False
    Left = 40
    Top = 8
  end
  object FDPhysMySQLDriverLink2: TFDPhysMySQLDriverLink
    VendorLib = 'E:\Projetos\WKTeste\Model\libmysql.dll'
    Left = 152
    Top = 8
  end
  object tbClientes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 40
    Top = 72
    object tbClientesCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object tbClientesNOME: TStringField
      FieldName = 'NOME'
      Size = 99
    end
    object tbClientesCIDADE: TStringField
      FieldName = 'CIDADE'
      Size = 80
    end
    object tbClientesUF: TStringField
      FieldName = 'UF'
      Size = 2
    end
  end
  object tbProdutos: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 128
    Top = 72
    object tbProdutosCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object tbProdutosDESCRICAO: TStringField
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object tbProdutosPRECOVENDA: TCurrencyField
      FieldName = 'PRECOVENDA'
    end
  end
  object tbPedidos: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 212
    Top = 72
    object tbPedidosNUMEROPEDIDO: TIntegerField
      FieldName = 'NUMEROPEDIDO'
    end
    object tbPedidosDATAEMISSAO: TDateField
      FieldName = 'DATAEMISSAO'
    end
    object tbPedidosVALORTOTAL: TCurrencyField
      FieldName = 'VALORTOTAL'
    end
    object tbPedidosCODIGOCLIENTE: TIntegerField
      FieldName = 'CODIGOCLIENTE'
    end
    object tbPedidosNOMECLIENTE: TStringField
      DisplayWidth = 80
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object tbPedidosIDGUID: TStringField
      FieldName = 'IDGUID'
      Size = 45
    end
  end
  object tbProdutosPedido: TFDMemTable
    FieldDefs = <>
    IndexDefs = <>
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    StoreDefs = True
    Left = 304
    Top = 72
    object tbProdutosPedidoNUMEROPEDIDO: TIntegerField
      FieldName = 'NUMEROPEDIDO'
    end
    object tbProdutosPedidoCODIGOPRODUTO: TIntegerField
      DisplayWidth = 20
      FieldName = 'CODIGOPRODUTO'
    end
    object tbProdutosPedidoQUANTIDADE: TIntegerField
      DisplayWidth = 15
      FieldName = 'QUANTIDADE'
    end
    object tbProdutosPedidoVLRUNITARIO: TCurrencyField
      DisplayWidth = 15
      FieldName = 'VLRUNITARIO'
    end
    object tbProdutosPedidoVLRTOTAL: TCurrencyField
      DisplayWidth = 20
      FieldName = 'VLRTOTAL'
    end
    object tbProdutosPedidoDESCRICAO: TStringField
      DisplayWidth = 60
      FieldName = 'DESCRICAO'
      Size = 100
    end
    object tbProdutosPedidoIDGUID: TStringField
      FieldKind = fkCalculated
      FieldName = 'IDGUID'
      Size = 50
      Calculated = True
    end
    object tbProdutosPedidoAUTOINCREM: TIntegerField
      FieldName = 'AUTOINCREM'
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 280
    Top = 8
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 376
    Top = 8
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection
    SQL.Strings = (
      'select * from VW_PEDIDOS')
    Left = 64
    Top = 168
    object FDQuery1NUMEROPEDIDO: TFDAutoIncField
      FieldName = 'NUMEROPEDIDO'
      Origin = 'NUMEROPEDIDO'
    end
    object FDQuery1DATAEMISSAO: TSQLTimeStampField
      AutoGenerateValue = arDefault
      FieldName = 'DATAEMISSAO'
      Origin = 'DATAEMISSAO'
    end
    object FDQuery1CODIGOCLIENTE: TIntegerField
      FieldName = 'CODIGOCLIENTE'
      Origin = 'CODIGOCLIENTE'
      Required = True
    end
    object FDQuery1VALORTOTAL: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'VALORTOTAL'
      Origin = 'VALORTOTAL'
      Precision = 10
      Size = 2
    end
    object FDQuery1NOMECLIENTE: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      Size = 100
    end
    object FDQuery1IDGUID: TStringField
      FieldName = 'IDGUID'
      Origin = 'IDGUID'
      Required = True
      Size = 45
    end
  end
end
