unit u_fPedidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Grids, Vcl.DBGrids,  Vcl.Buttons,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  u_TControleCliente, u_TControleProdutos, u_TControlePedidos, System.ImageList,
  Vcl.ImgList;

type
  // classes
  TFNs = class
   class function GridImagem(grid : TDBGrid; const Rect : TRect;
      DataCol : Integer; Column : TColumn; State : TGridDrawState;
      SN : string; img16 : TImageList; idImgS, idImgN : integer):Boolean;
  end;

  Tfrm_Pedidos = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    dbGrid: TDBGrid;
    spbFiltro: TSpeedButton;
    rgFiltrar: TRadioGroup;
    spbVisualizar: TSpeedButton;
    spbEdit: TSpeedButton;
    spbAdd: TSpeedButton;
    ds1: TDataSource;
    ImageList1: TImageList;
    procedure spbAddClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure spbVisualizarClick(Sender: TObject);
    procedure dbGridDblClick(Sender: TObject);
    procedure dbGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dbGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbGridCellClick(Column: TColumn);
  private
    FClientes: TControleCliente;
    FProdutos: TControleProdutos;
    FPedidos: TControlePedidos;
    FProcesso: Boolean;
    { Private declarations }
  public
    { Public declarations }
    property Clientes : TControleCliente read FClientes write FClientes;
    property Produtos : TControleProdutos read FProdutos write FProdutos;
    property Pedidos  : TControlePedidos read  FPedidos write FPedidos;
    property Processo : Boolean read FProcesso write FProcesso;

    procedure TerminoBusca(Sender : TObject);

  end;

var
  frm_Pedidos: Tfrm_Pedidos;

implementation

{$R *.dfm}

uses u_fPedido;

procedure Tfrm_Pedidos.dbGridCellClick(Column: TColumn);
begin
   if (Self.ds1.DataSet.RecordCount > 0) then
   begin
     case Column.Index of
      6 : begin
           // Primeiro indique qual coluna do grid voce quer se posicionar.
           dbGrid.SelectedIndex := 0;
           dbGrid.SetFocus;
           Self.spbVisualizar.Click;
      end;
     end;
   end;
end;

procedure Tfrm_Pedidos.dbGridDblClick(Sender: TObject);
begin
 // Self.spbVisualizar.Click;
end;

procedure Tfrm_Pedidos.dbGridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'visualizar' then
   TFNs.GridImagem(dbGrid,Rect,DataCol,Column,State,'S',ImageList1,1,2);
end;

procedure Tfrm_Pedidos.dbGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Self.spbVisualizar.Click;
end;

procedure Tfrm_Pedidos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if  Assigned(FClientes) then FreeAndNil(FClientes);
end;

procedure Tfrm_Pedidos.FormCreate(Sender: TObject);
begin
  if not Assigned(FClientes) then
     FClientes  := TControleCliente.Create;
  if not Assigned(FProdutos) then
     FProdutos := TControleProdutos.Create;
  if not Assigned(FPedidos) then
     FPedidos := TControlePedidos.Create;
end;

procedure Tfrm_Pedidos.FormShow(Sender: TObject);
begin
  // Carregando clientes
  with Clientes do begin
     Parametros.Clear;
     AddParametro('tipo','lista');
     AddParametro('orderby','nome');
     GetLista;
  end;
  // carregando produtos
  with Produtos do begin
     Parametros.Clear;
     AddParametro('tipo','lista');
     AddParametro('orderby','descricao');
     GetLista;
  end;
  // pedidos
  with Pedidos do begin
     Parametros.Clear;
     AddParametro('tipo','lista');
    // AddParametro('orderby','descricao');
     GetLista;
  end;
  

 // Self.ds1.DataSet := Clientes.PMemTable;
  Self.ds1.DataSet := Pedidos.PMemTable;
end;

procedure Tfrm_Pedidos.spbAddClick(Sender: TObject);
var
  f : Tfrm_Pedido;
begin
   f := Tfrm_Pedido.Create(Self);
   try
     f.Clientes := FClientes;
     f.Produtos := FProdutos;
     f.Pedidos  := FPedidos;
     f.Status   := 'insert';
     Pedidos.Novo;
     f.ShowModal;
   finally
      if Assigned(f) then FreeAndNil(f);

      with Pedidos do begin
         Parametros.Clear;
         AddParametro('tipo','lista');
         GetLista;
      end;
   end;
end;

procedure Tfrm_Pedidos.spbVisualizarClick(Sender: TObject);
var
  f : Tfrm_Pedido;
  idc : integer;
  thFiltro : TThread;
begin

   if Pedidos.PMemTable.RecordCount > 0 then
   begin
     idc := Pedidos.PMemTable.FieldByName('NUMEROPEDIDO').AsInteger;

     if not Assigned(frm_Pedido) then
       f := Tfrm_Pedido.Create(Self);
     Self.spbVisualizar.Enabled := False;
     Self.spbFiltro.Enabled     := false;
     try
      f.Status := 'browse';
      Pedidos.Sincroniza;
      Pedidos.ItensPedido.Default;
      Pedidos.ItensPedido.Parametros.Clear;
      Pedidos.ItensPedido.AddParametro('tipo','lista');
      Pedidos.ItensPedido.AddParametro('numeropedido',Pedidos.IntToText(idc));
      Pedidos.ItensPedido.GetLista;
      f.Clientes := FClientes;
      f.Produtos := FProdutos;
      f.Pedidos  := FPedidos;
      f.ShowModal;

     finally
       if Assigned(f) then FreeAndNil(f);

      Self.ds1.DataSet.DisableControls;
      Processo := True;
         ThFiltro := TThread.CreateAnonymousThread(
         procedure ()
         var
           i : integer;
         begin
                if Pedidos.GetLista <> 'n' then
                begin
                 // colocar um sincronyzer aqui
                 TThread.Synchronize(
                    TThread.CurrentThread,
                    procedure ()   // procedure anonima para thread anonima
                    begin
                         Self.Pedidos.PMemTable.First;
                        // Self.Exibir(Sender);
                    end
                 );
                end;
         end
       );
       // finalizando a thread
       ThFiltro.FreeOnTerminate := True;
       ThFiltro.OnTerminate     := TerminoBusca;
       ThFiltro.Start;
     end;
   end
    else
      MessageDlg('N�o ha produto para visualizar!',mtInformation,[mbOK],0,mbOK);



end;

procedure Tfrm_Pedidos.TerminoBusca(Sender: TObject);
begin
  Sleep(200);
  Self.spbVisualizar.Enabled := True;
  Self.spbFiltro.Enabled     := True;
  Self.ds1.DataSet.EnableControls;
  Processo := False;
end;

{ TFn }

class function TFNs.GridImagem(grid: TDBGrid; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState; SN: string;
  img16: TImageList; idImgS, idImgN: integer): Boolean;
begin
  try
   with grid do
   begin
    DefaultDrawDataCell(Rect, Columns[DataCol].Field, State);
    Canvas.FillRect(Rect);
    DefaultDrawColumnCell(Rect, DataCol,Column, State);

    // imagem padr�o - deve ter uma imagem padr�o no imaglist16
    img16.Draw(Canvas, Rect.Left +(Rect.Width div 2)- 8, Rect.Top + 1, 1);

    // imagem para true
    if SN = 'S' then
     img16.Draw(Canvas, Rect.Left +(Rect.Width div 2)- 8, Rect.Top + 1, idImgS)
    else
    if SN = 'N' then
     img16.Draw(Canvas, Rect.Left +(Rect.Width div 2)- 8, Rect.Top + 1, idImgN);
   end;
 except

 end;
end;

end.
