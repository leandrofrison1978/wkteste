unit u_fAddItemPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls,
  u_TControleProdutos, u_TControlePedidos;

type
  Tf_AddProduto = class(TForm)
    Panel4: TPanel;
    Panel1: TPanel;
    eCodigo: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    eQt: TEdit;
    Label3: TLabel;
    eVlrUnitario: TEdit;
    Label4: TLabel;
    eDescricao: TEdit;
    Label5: TLabel;
    eTotal: TEdit;
    btConfirmar: TBitBtn;
    procedure eCodigoChange(Sender: TObject);
    procedure eQtChange(Sender: TObject);
    procedure btConfirmarClick(Sender: TObject);
    procedure eCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eVlrUnitarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eQtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FProdutos: TControleProdutos;
    FPedidos: TControlePedidos;
    FStatus: string;
    { Private declarations }
    function CalcValor(const Preco, Qt : string):string;
  public
    { Public declarations }
    property Produtos : TControleProdutos read FProdutos write FProdutos;
    property Pedidos  : TControlePedidos read  FPedidos write FPedidos;
    property Status : string read FStatus write FStatus;
  end;

var
  f_AddProduto: Tf_AddProduto;

implementation

{$R *.dfm}

procedure Tf_AddProduto.btConfirmarClick(Sender: TObject);
begin
   Self.Status := 'added';
   Self.Close;
end;

function Tf_AddProduto.CalcValor(const Preco, Qt: string): string;
var
  vl, qtd : Currency;
begin
   Result := '0';
   try
     if not(Preco.IsEmpty) then
       if not(Qt.IsEmpty) then
       begin
         vl      := StrToCurr(Preco);
         qtd     := StrToCurr(Qt);
         Result  := CurrToStr(vl * qtd);
       end;
   except

   end;
end;

procedure Tf_AddProduto.eCodigoChange(Sender: TObject);
begin

  if Produtos.LocalizaProduto(Produtos.TextToInt(eCodigo.Text)) then
  begin
    Produtos.Sincroniza;
    eDescricao.Text    := Produtos.Produto.Descricao;
    eVlrUnitario.Text  := CurrToStr(Produtos.Produto.PrecoVenda);
  end
  else
    begin
      eDescricao.Text     := '';
      eVlrUnitario.Text   := '0,00';
      eTotal.Text         := '0,00';
    end;
end;

procedure Tf_AddProduto.eCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Perform(WM_NEXTDLGCTL,0,0);
end;

procedure Tf_AddProduto.eQtChange(Sender: TObject);
begin
   eTotal.Text  := Self.CalcValor(eVlrUnitario.Text,eQt.Text);
end;
procedure Tf_AddProduto.eQtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then
  Perform(WM_NEXTDLGCTL,0,0);
end;

procedure Tf_AddProduto.eVlrUnitarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_RETURN then
    Perform(WM_NEXTDLGCTL,0,0);
end;

end.


