object f_AddProduto: Tf_AddProduto
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Adicionar Produto'
  ClientHeight = 291
  ClientWidth = 444
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 438
    Height = 46
    Align = alTop
    Caption = 'Pedido de Vendas'
    TabOrder = 0
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 55
    Width = 438
    Height = 233
    Align = alClient
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    object Label1: TLabel
      Left = 33
      Top = 18
      Width = 89
      Height = 13
      Caption = 'C'#243'digo do produto'
    end
    object Label2: TLabel
      Left = 33
      Top = 99
      Width = 56
      Height = 13
      Caption = 'Quantidade'
    end
    object Label3: TLabel
      Left = 33
      Top = 58
      Width = 64
      Height = 13
      Caption = 'Valor Unit'#225'rio'
    end
    object Label4: TLabel
      Left = 33
      Top = 139
      Width = 102
      Height = 13
      Caption = 'Descri'#231#227'o do produto'
    end
    object Label5: TLabel
      Left = 224
      Top = 99
      Width = 51
      Height = 13
      Caption = 'Valor Total'
    end
    object eCodigo: TEdit
      Left = 33
      Top = 32
      Width = 169
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      TextHint = 'C'#243'digo do produto'
      OnChange = eCodigoChange
      OnKeyDown = eCodigoKeyDown
    end
    object eQt: TEdit
      Left = 33
      Top = 113
      Width = 169
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      TextHint = 'Quantidade'
      OnChange = eQtChange
      OnKeyDown = eQtKeyDown
    end
    object eVlrUnitario: TEdit
      Left = 33
      Top = 72
      Width = 169
      Height = 21
      TabOrder = 1
      TextHint = 'Valor unit'#225'rio'
      OnKeyDown = eVlrUnitarioKeyDown
    end
    object eDescricao: TEdit
      Left = 33
      Top = 154
      Width = 360
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 4
      TextHint = 'Valor unit'#225'rio'
    end
    object eTotal: TEdit
      Left = 224
      Top = 113
      Width = 169
      Height = 21
      TabStop = False
      ReadOnly = True
      TabOrder = 5
      TextHint = 'Valor unit'#225'rio'
    end
    object btConfirmar: TBitBtn
      Left = 224
      Top = 30
      Width = 169
      Height = 25
      Caption = '&Confirmar'
      TabOrder = 3
      OnClick = btConfirmarClick
    end
  end
end
