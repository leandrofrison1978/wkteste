unit u_fPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons, Vcl.ExtCtrls, u_TControleCliente, u_TControlePedidos,
  u_TControleProdutos, u_TControleProdutosPedido, Vcl.ComCtrls;

type
  Tfrm_Pedido = class(TForm)
    Panel1: TPanel;
    spbVisualizar: TSpeedButton;
    spbEdit: TSpeedButton;
    spbAdd: TSpeedButton;
    Panel3: TPanel;
    grid: TDBGrid;
    Panel2: TPanel;
    Panel4: TPanel;
    lbTotal: TLabel;
    cbCliente: TComboBox;
    Label2: TLabel;
    ds1: TDataSource;
    spbCancelarItem: TSpeedButton;
    spbSalvar: TSpeedButton;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    eNumPedido: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    eCodCli: TEdit;
    Label5: TLabel;
    eNomeCli: TEdit;
    eData: TDateTimePicker;
    procedure spbAddClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure spbSalvarClick(Sender: TObject);
    procedure cbClienteSelect(Sender: TObject);
    procedure gridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FClientes: TControleCliente;
    FProdutos: TControleProdutos;
    FPedidos: TControlePedidos;
    FStatus: string;
    { Private declarations }
    procedure Exibir;
  public
    { Public declarations }
    property Clientes : TControleCliente read FClientes write FClientes;
    property Produtos : TControleProdutos read FProdutos write FProdutos;
    property Pedidos  : TControlePedidos read  FPedidos write FPedidos;
    property Status : string read FStatus write FStatus;

    function GravarNaClass: Boolean;
  end;

var
  frm_Pedido: Tfrm_Pedido;

implementation

{$R *.dfm}

uses u_fAddItemPedido;

procedure Tfrm_Pedido.cbClienteSelect(Sender: TObject);
begin
  with Clientes do begin
    if cbCliente.ItemIndex >= 0 then
      if Localizar('NOME',cbCliente.Text) then
      begin
         Sincroniza;
         eCodCli.Text   := IntToText(Cliente.Codigo);
         eNomeCli.Text  := Cliente.Nome;
      end;

  end;
end;

procedure Tfrm_Pedido.Exibir;
begin
   with Pedidos.Pedido do begin
    eNumPedido.Text := Pedidos.IntToText(NumPedido);
    eData.Date      := DataEmissao;
    eCodCli.Text    := Pedidos.IntToText(CodigoCliente);
    eNomeCli.Text   := Cliente;
    lbTotal.Caption := Pedidos.CurrToText(Pedidos.ItensPedido.Total,
                                            'Valor Total do Pedido:');

   end;
end;

procedure Tfrm_Pedido.FormShow(Sender: TObject);
begin
   Clientes.CarregarCombo(cbCliente,'nome');
   Self.ds1.DataSet := Pedidos.ItensPedido.PMemTable;
   Self.Exibir;
end;

function Tfrm_Pedido.GravarNaClass: Boolean;
begin

  try
    with Pedidos.Pedido do begin
      Default;
      NumPedido     := Pedidos.TextToInt(eNumPedido.Text);
      DataEmissao   := eData.Date;
      CodigoCliente := Pedidos.TextToInt(eCodCli.Text);
      ValotTotal    := Pedidos.ItensPedido.Total;

      Result := True;
    end;
  except
    Result := false;
  end;
end;

procedure Tfrm_Pedido.gridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
const
  msgConfirm : string ='Deseja realmente excluir o registro selecionado!';
  msgFalha : string = 'Falha ao deletar o registro!';
begin
  if Key = VK_DELETE then
  begin

    if Self.ds1.DataSet.RecNo >= 0 then begin

      if MessageDlg(msgConfirm,mtConfirmation,[mbYes,mbNo],0,mbNo) = IDYES then
      begin
        if not Pedidos.ItensPedido.Delete then
          ShowMessage(msgFalha)
        else
          lbTotal.Caption := Pedidos.CurrToText(Pedidos.ItensPedido.Total,
                                            'Valor Total do Pedido: ');
      end;

    end;
  end;
end;

procedure Tfrm_Pedido.spbAddClick(Sender: TObject);
var
  f : Tf_AddProduto;
begin
   f := Tf_AddProduto.Create(Self);
   try
     f.Produtos := FProdutos;
     f.Status   := 'insert';
     f.ShowModal;
     // verificando o retorno para adicionar na table
     if f.Status = 'added' then
        Pedidos.ItensPedido.AddProduto(f.eCodigo.Text,f.eQt.Text,
                                       f.eVlrUnitario.Text,
                                       f.eTotal.Text,f.eDescricao.Text);
        lbTotal.Caption := Pedidos.CurrToText(Pedidos.ItensPedido.Total,'Valor Total do Pedido:');
   finally
      if Assigned(f) then FreeAndNil(f);

   end;

end;

procedure Tfrm_Pedido.spbSalvarClick(Sender: TObject);
begin
   // adicionar o produto na classe base
   if Self.GravarNaClass then
    if Pedidos.SalvarRegistro then
    begin
      ShowMessage('Pedido salvo com sucesso.');
      Self.Status := 'added';
      Close;
    end;
end;

end.
